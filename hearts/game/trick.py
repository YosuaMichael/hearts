from hearts.game.card import Card

import hearts.common.util as util

NUM_PLAYER = 4


class HeartTrick:
    def __init__(self, players, first_player,
            is_first_trick=False, is_heart_broken=False):
        self.players = players
        self.players_position = {players[i]: i for i in range(NUM_PLAYER)}
        self.first_player = first_player
        self.first_player_position = self.players_position.get(first_player)
        self.is_first_trick = is_first_trick
        self.cards = {}
        self.suit = None
        self.is_heart_broken = is_heart_broken
        self.winner = None
        self.point = None
        self.cause_broken_heart = None
        self.logger = util.create_logger(__name__)

    def play(self, state_obs=None):
        player_seq = [self.players[(self.first_player_position + i) % 4]
                for i in range(NUM_PLAYER)]

        # Handling the first play
        cur_player = player_seq[0]
        restriction = cur_player.hand
        if self.is_first_trick:
            # First player must play two club
            restriction = [Card("2_C")]
        elif not self.is_heart_broken:
            restriction = [card for card in cur_player.hand
                    if card.get_suit() != "H"]
        if len(restriction) == 0:
            restriction = cur_player.hand
        # Player choose card
        state_data = None
        if state_obs is not None:
            state_data = state_obs.generate_state_data(cur_player, restriction)
        chosen_card = cur_player.choose_card(restriction, state_data)
        # Save the state on the HeartTrick object
        self.cards[cur_player] = chosen_card
        self.suit = chosen_card.get_suit()

        # Handling the second to fourth play
        for i in range(1, NUM_PLAYER):
            cur_player = player_seq[i]
            restriction = [card for card in cur_player.hand
                    if card.get_suit() == self.suit]
            if len(restriction) == 0:
                # In case the player dont have any card with the same suit
                # the player can use any other suit unless it is on the first trick
                restriction = cur_player.hand
                if self.is_first_trick:
                    restriction = [card for card in cur_player.hand
                            if (card.get_suit() != "H" and card.get_code() != "Q_S")]
            # Player choose card
            if state_obs is not None:
                state_data = state_obs.generate_state_data(cur_player, restriction)
            chosen_card = cur_player.choose_card(restriction, state_data)
            # Save the state
            self.cards[cur_player] = chosen_card

        # Get winner and point
        self.winner = self.get_winner()
        self.point = self.get_point()
        self.cause_broken_heart = self.are_breaking_heart()

        return self.winner, self.point, self.cause_broken_heart

    def get_winner(self):
        self.logger.debug(self.cards)
        same_suit_cards = [x for x in self.cards.items() if x[1].get_suit() == self.suit]
        winner = sorted(same_suit_cards, key=lambda x: x[1].get_val_rank())[-1][0]
        return winner

    def get_point(self):
        num_point = 0
        for card in self.cards.values():
            if card.get_code() == "Q_S":
                num_point += 13
            elif card.get_suit() == "H":
                num_point += 1
        return num_point

    def are_breaking_heart(self):
        for card in self.cards.values():
            if card.get_suit() == "H":
                return True
            # using queen of spades considered as breaking hearts
            if card.get_code() == "Q_S":
                return True
        return False


if __name__ == "__main__":
    import hearts.game.input_handler as input_handler
    from hearts.game.player import Player
    from hearts.game.deck import Deck

    deck = Deck.create_standard_deck()
    deck.shuffle()
    hand_cards = deck.divide_deck(num_player=4)

    players = []
    for i in range(NUM_PLAYER):
        ih = input_handler.TerminalInputHandler()
        player = Player(player_id="{:03d}".format(i),
                name="player_{}".format(i), input_handler=ih)
        player.hand.init_hand(hand_cards[i])
        players.append(player)

    ht = HeartTrick(
            players=players,
            first_player=players[0],
            is_first_trick=False,
            is_heart_broken=False
    )

    winner, point, cause_broken_heart = ht.play()






