from hearts.game.card import CardGenerator
import hearts.common.util as util

import random


class Deck:
    def __init__(self, cards):
        self.cards = cards

    @staticmethod
    def create_standard_deck(use_joker=False):
        card_generator = CardGenerator()
        if use_joker:
            return Deck(cards=card_generator.generate_54_cards())
        else:
            return Deck(cards=card_generator.generate_52_cards())

    def shuffle(self):
        random.shuffle(self.cards)
        return True

    def pick_card(self, i=-1):
        # Get card at i_th position from the top
        card = None
        if len(self.cards) == 0:
            return card
        if i == -1 or i >= len(self.cards):
            card = self.cards.pop()
        elif i >= 0 and i < len(self.cards):
            card = self.cards[i]
            del self.cards[i]
        return card

    def add_card(self, card, i=-1):
        # Add card at i_th position from the top
        if i == -1 or i >= len(self.cards):
            self.cards.append(card)
        elif i >= 0 and i < len(self.cards):
            self.cards.insert(i, card)
        return True

    def divide_deck(self, num_player=4):
        num_per_hand = len(self.cards) // num_player
        hand_cards = []
        for i in range(num_player):
            hand_cards.append(util.sort_cards(self.cards[i*num_per_hand:(i+1)*num_per_hand]))
        self.cards.clear()
        return hand_cards

    # Overload operators
    def __str__(self):
        return str(util.sort_cards(self.cards))

    def __repr__(self):
        return str(util.sort_cards(self.cards))

    def __contains__(self, card):
        return card in self.cards

    def __iter__(self):
        self._iter_obj = iter(self.cards)
        return self

    def __next__(self):
        return next(self._iter_obj)

    def __len__(self):
        return len(self.cards)

    def __getitem__(self, index):
        return self.cards[index]


if __name__ == "__main__":
    d = Deck.create_standard_deck()
    logger = util.create_logger(__name__)
    logger.info(d)
