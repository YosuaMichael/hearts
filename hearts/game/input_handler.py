from hearts.game.card import Card
import hearts.common.util as util


class InputHandler:
    def __init__(self):
        self.player = None
        self.bot = None
        self.logger = util.create_logger(__name__)

    def attach_player(self, player):
        self.player = player

    def attach_bot(self, bot):
        self.bot = bot

    def input_one_card(self, restriction, state_data={}):
        chosen_card = self.choose_one_card(restriction, state_data)
        if chosen_card not in restriction:
            raise Exception("[ERROR] Must chose a valid card!")
            exit(1)
        return chosen_card

    def input_multi_cards(self, n=3):
        chosen_cards = self.choose_multi_cards(self.player.hand.cards, n)
        if (
                len(set(chosen_cards)) != n or
                (not self._inside(chosen_cards, self.player.hand))
        ):
            raise Exception("[ERROR] Must chose valid cards!")
            exit(1)
        return chosen_cards

    def ready_for_new_round(self, round_num):
        # The InputHandler lifetime is as long as player
        # In this case it will live on one game of hearts
        # The bot might want to get notified when new round is started
        # For example the bot might want to clean up and reset some data
        pass

    def choose_one_card(self, restriction, state_data={}):
        # Need to overwrite as action to play one card in a trick
        # It must return one card amont the restriction
        raise NotImplementedError("input_one_card is not implemented!")

    def choose_multi_cards(self, restriction, n=3):
        # Need to overwrite as action to exchange card
        # It must return n cards from player hand
        raise NotImplementedError("input_cards is not implemented!")

    def _inside(self, cards1, cards2):
        return len(set(cards1) - set(cards2)) == 0


class TerminalInputHandler(InputHandler):
    def __init__(self):
        pass

    def choose_one_card(self, restriction, state_data={}):
        print("state_data: {}".format(state_data))
        print("choose card from restriction: {}".format(
            util.sort_cards(restriction)
        ))
        chosen_card_code = input().strip()

        # We also check user input here!
        while not Card(chosen_card_code) in restriction:
            print("Chose a valid card!")
            chosen_card_code = input().strip()
        return Card(chosen_card_code)

    def choose_multi_cards(self, restriction, n=3):
        print("{} choose {} cards from: {}".format(self.player.name, n, str(restriction)))
        chosen_cards = [Card(card_code.strip()) for card_code in input().split(",")]

        while len(set(chosen_cards)) != n or not self._inside(chosen_cards, restriction):
            print("Chose valid {} cards from hand!".format(n))
            chosen_cards = [Card(card_code.strip()) for card_code in input().split(",")]

        return chosen_cards


class BotInputHandler(InputHandler):
    def choose_one_card(self, restriction, state_data={}):
        chosen_card = self.bot.choose_one_card(restriction, state_data)
        # Print to debug
        print("[BOT] {} play {}".format(self.player.name, chosen_card))
        return chosen_card

    def choose_multi_cards(self, restriction, n=3):
        chosen_cards = self.bot.choose_multi_cards(restriction, n)
        return chosen_cards

    def ready_for_new_round(self, round_num):
        # Just to notify the bot that a new round is starting
        self.bot.ready_for_new_round(round_num)



