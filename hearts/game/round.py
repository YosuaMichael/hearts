from hearts.game.card import Card
from hearts.game.deck import Deck
from hearts.game.player import Player
from hearts.game.trick import HeartTrick

import hearts.common.util as util

NUM_PLAYER = 4


class HeartRound:
    def __init__(self, players, round_num):
        # Initiate the players
        self.players = players
        self.scores = {player: 0 for player in self.players}
        self.positions = {self.players[i]: i for i in range(len(self.players))}

        # Initiate other variable
        self.round_num = round_num
        self.cur_trick = None
        self.trick_history = []
        self.state_obs = None
        self.logger = util.create_logger(__name__)

    def attach_state_observer(self, state_obs):
        self.state_obs = state_obs
        self.state_obs.attach_round(self)

    def initiate(self):
        # Initiate decks
        deck = Deck.create_standard_deck()
        deck.shuffle()

        # Initiate hand
        hand_cards = deck.divide_deck(num_player=NUM_PLAYER)
        for i in range(NUM_PLAYER):
            self.players[i].hand.init_hand(hand_cards[i])

        # state observe initial hand
        if self.state_obs is not None:
            self.state_obs.observe_initial_hand()

    def exchange_phase(self):
        exchange_pairs = []
        if self.round_num % 4 == 0:
            exchange_pairs = [(0, 1), (1, 2), (2, 3), (3, 0)]
        elif self.round_num % 4 == 1:
            exchange_pairs = [(0, 3), (3, 2), (2, 1), (1, 0)]
        elif self.round_num % 4 == 2:
            exchange_pairs = [(0, 2), (2, 0), (1, 3), (3, 1)]
        else:
            return True

        # Get card to be given
        exchanged_cards = [self.players[i].choose_cards(n=3) for i in range(NUM_PLAYER)]

        # Give the card to the other player
        for exchange_pair in exchange_pairs:
            giver, receiver = exchange_pair
            self.players[receiver].hand.receive_cards(exchanged_cards[giver])

        # state observer exchanges
        if self.state_obs is not None:
            self.state_obs.observe_exchanges(exchanged_cards, exchange_pairs)

        return True

    def play_round(self):
        self.logger.debug("START INITIATING")

        self.initiate()

        self.logger.debug("AFTER INITIATING")
        for i in range(NUM_PLAYER):
            self.logger.debug("{}:{}".format(self.players[i], str(self.players[i].hand)))
        self.logger.debug("START EXCHANGE")

        self.exchange_phase()

        self.logger.debug("AFTER EXCHANGE")
        for i in range(NUM_PLAYER):
            self.logger.debug("{}:{}".format(self.players[i], str(self.players[i].hand)))

        # Play first trick
        def get_first_player(players):
            two_club = Card("2_C")
            for player in players:
                if two_club in player.hand:
                    return player
            return None

        # Init state for first play
        is_heart_broken = False
        first_player = get_first_player(self.players)

        cur_trick = HeartTrick(self.players, first_player,
                is_first_trick=True, is_heart_broken=is_heart_broken)

        # Observe the ongoing trick
        if self.state_obs is not None:
            self.state_obs.attach_ongoing_trick(cur_trick)

        winner, point, cause_broken_heart = cur_trick.play(self.state_obs)
        if cause_broken_heart:
            self.logger.error("[ERROR] The first trick shouldnt be allowed to broke the heart")
            is_heart_broken = True
            if self.state_obs is not None:
                self.state_obs.observe_heart_breaking_event()
        first_player = winner
        self.scores[winner] += point
        self.logger.info(winner, point, cause_broken_heart, self.scores)
        if point > 0:
            self.logger.error("[ERROR] The first trick shouldnt have point!")
            exit(0)
        self.trick_history.append(cur_trick)

        # Play the rest
        for i in range(1, 13):
            cur_trick = HeartTrick(self.players, first_player,
                    is_first_trick=False, is_heart_broken=is_heart_broken)

            # Observe the ongoing trick
            if self.state_obs is not None:
                self.state_obs.attach_ongoing_trick(cur_trick)

            winner, point, cause_broken_heart = cur_trick.play(self.state_obs)
            if cause_broken_heart:
                is_heart_broken = True
                if self.state_obs is not None:
                    self.state_obs.observe_heart_breaking_event()
            first_player = winner
            self.scores[winner] += point
            self.logger.info(winner, point, cause_broken_heart, self.scores)
            self.trick_history.append(cur_trick)

        # Check in case of shooting the moon
        has_shooting = False
        shooting_player = None
        for player, score in self.scores.items():
            if score == 26:
                has_shooting = True
                shooting_player = player

        if has_shooting:
            # Adjusting the score
            self.logger.debug("HAS SHOOTING THE MOON!")
            for player in self.scores:
                if player == shooting_player:
                    self.scores[player] = 0
                else:
                    self.scores[player] = 26

        self.logger.info("ROUND_SCORES: {}".format(self.scores))
        return self.scores


if __name__ == "__main__":
    import hearts.game.input_handler as input_handler
    from hearts.game.state import State

    players = []
    for i in range(NUM_PLAYER):
        ih = input_handler.TerminalInputHandler()
        player = Player(player_id=i, name="name_{}".format(i), input_handler=ih)
        players.append(player)

    hr = HeartRound(players, round_num=3)
    state_obs = State()
    hr.attach_state_observer(state_obs)
    hr.play_round()


