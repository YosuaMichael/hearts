from hearts.game.hand import Hand


class Player:
    def __init__(self, player_id, name, input_handler):
        self.id = player_id
        self.name = name
        self.hand = Hand()
        self.input_handler = input_handler
        self.input_handler.attach_player(self)

    def get_hand(self):
        return self.hand

    def choose_card(self, restriction, state_data={}):
        chosen_card = self.input_handler.input_one_card(
                restriction, state_data)
        return self.hand.throw_card(chosen_card)

    def choose_cards(self, n=3, state_data={}):
        chosen_cards = self.input_handler.input_multi_cards(n)
        return self.hand.throw_cards(chosen_cards)

    def ready_for_new_round(self, round_num):
        # Make sure everyone is ready for new round
        self.input_handler.ready_for_new_round(round_num)

    # Overload operator
    def __repr__(self):
        return self.name

    def __str__(self):
        return str(self.id)

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, p):
        return self.id == p.id


