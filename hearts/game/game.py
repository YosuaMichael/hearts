from hearts.game.round import HeartRound
from hearts.game.state import State

import hearts.common.util as util

class HeartGame:
    def __init__(self, players, max_score=100):
        self.players = players
        self.scores = {player: 0 for player in self.players}
        self.max_score = max_score

        self.round_history = []
        self.logger = util.create_logger(__name__)

    def update_score(self, round_scores):
        for player in round_scores:
            self.scores[player] += round_scores[player]

    def play(self):
        round_num = 0
        while max(self.scores.values()) < self.max_score:
            for player in self.players:
                # Notify the bot to get ready for a new round
                player.ready_for_new_round(round_num)
            self.logger.info("ROUND {}".format(round_num))
            hr = HeartRound(self.players, round_num)
            # attach state observer
            state_obs = State()
            hr.attach_state_observer(state_obs)
            state_obs.observe_total_score(self.scores)

            round_scores = hr.play_round()
            self.update_score(round_scores)
            self.round_history.append(hr)
            round_num += 1

            self.logger.info("GAME_SCORES: {}".format(self.scores))

        ordered_scores = sorted(self.scores.items(), key=lambda x: x[1])
        self.logger.info(ordered_scores)


if __name__ == "__main__":
    import hearts.game.input_handler as input_handler
    from hearts.game.player import Player
    NUM_PLAYER = 4

    players = []
    for i in range(NUM_PLAYER):
        ih = input_handler.TerminalInputHandler()
        player = Player(player_id=i, name="name_{}".format(i), input_handler=ih)
        players.append(player)

    hg = HeartGame(players, max_score=27)
    hg.play()

