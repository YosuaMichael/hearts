import hearts.common.util as util

class State:
    def __init__(self):
        # Linking to the heart_round object
        self.heart_round = None
        # Linking to the object owned by round
        self.players = None
        self.trick_history = None

        # Will store player's position
        self.player_position = None

        # Will store the total score until the latest completed round
        self.total_score = {}

        # Will store the exchange history of each player
        self.exchanges = {}

        # Will store the initial hands
        self.initial_hands = None

        # Will store the ongoing trick (will be attached)
        self.ongoing_trick = None

        # Will store the heart breaking event
        self.is_heart_broken = False
        
        # Initiate other variable
        self.logger = util.create_logger(__name__)
        

    def attach_round(self, heart_round):
        self.heart_round = heart_round
        self.players = self.heart_round.players
        self.trick_history = self.heart_round.trick_history
        self.player_position = {
            self.players[i]: i for i in range(len(self.players))
        }
        self.logger.debug("[STATE] round {} attached!".format(self.heart_round.round_num))

    def observe_total_score(self, total_score):
        self.total_score = total_score.copy()
        self.logger.debug("[STATE] total score observed!")

    def observe_initial_hand(self):
        players = self.players
        self.initial_hands = {
            players[i]: players[i].hand.cards.copy()
            for i in range(len(players))
        }
        self.logger.debug("[STATE] initial hands observed!")
        self.logger.debug(self.initial_hands)

    def observe_exchanges(self, exchanged_cards, exchange_pairs):
        players = self.players
        for player in players:
            self.exchanges[player] = {}
        for exchange_pair in exchange_pairs:
            giver_pos, receiver_pos = exchange_pair
            giver, receiver = players[giver_pos], players[receiver_pos]
            cards = exchanged_cards[giver_pos]
            # Record the giver
            self.exchanges[giver]["give"] = cards
            self.exchanges[giver]["give_to"] = receiver.id
            # Record the receiver
            self.exchanges[receiver]["receive"] = cards
            self.exchanges[receiver]["receive_from"] = giver.id
        self.logger.debug("[STATE] exchanges observed")

    def attach_ongoing_trick(self, heart_trick):
        self.ongoing_trick = heart_trick
        self.logger.debug("[STATE] ongoing trick attached")

    def observe_heart_breaking_event(self):
        self.is_heart_broken = True

    def generate_state_data(self, cur_player, restriction):
        state = {
            "private": {
                "player_id": cur_player.id,
                "player_pos": self.player_position.get(cur_player),
                "initial_hand": self.initial_hands.get(cur_player),
                "exchange": self.exchanges.get(cur_player),
            },
            "public": {
                "players": [player.id for player in self.players],
                "game_score": [self.total_score.get(player,0)
                    for player in self.players],
                "round_score": [self.heart_round.scores.get(player,0)
                    for player in self.players],
                "round_num": self.heart_round.round_num,
                "trick_history": [
                    {
                        "first_player": trick.first_player.id,
                        "cards": [trick.cards[player]
                            for player in self.players],
                        "suit": trick.suit,
                        "winner": trick.winner.id,
                        "point": trick.point,
                        "cause_broken_heart": trick.cause_broken_heart
                    }
                    for trick in self.trick_history
                ],
                "is_heart_broken": self.is_heart_broken,
            },
            "ongoing_trick": {
                "first_player": self.ongoing_trick.first_player.id,
                "cards": [self.ongoing_trick.cards.get(player, None)
                    for player in self.players],
                "cur_hand": cur_player.hand.cards.copy(),
                "restriction": restriction,
            },
        }
        return state



