class CardGenerator:
    def __init__(self):
        self.num2card = {}
        self.card2num = {}
        num = 0
        # Add normal card
        for suit in ["S","H","C","D"]:
            for val in ["A","2","3","4","5","6","7","8","9","10","J","Q","K"]:
                card = "{}_{}".format(val, suit)
                self.num2card[num] = card
                self.card2num[card] = num
                num += 1

        # Add jokers card
        for suit in ["R", "B"]:
            card = "{}_{}".format("Z",suit)
            self.num2card[num] = card
            self.card2num[card] = num
            num += 1

    def generate_card(self, num):
        code = self.num2card[num]
        return Card(code)

    def generate_52_cards(self):
        return [self.generate_card(i) for i in range(0,52)]

    def generate_54_cards(self):
        return [self.generate_card(i) for i in range(0,54)]
    

VAL_RANK = ["2","3","4","5","6","7","8","9","10","J","Q","K", "A"]
VAL_MAP = {VAL_RANK[i]:i for i in range(len(VAL_RANK))}

class Card:
    def __init__(self, code):
        # The code in the form of [val]_[suit]
        # val choices: A, 2, 3, 4, 5, 6, 7, 8, 9, 10, J, Q, K, Z(joker)
        # suit choices: S (spade), H (heart), C (clover), D (diamond), R (red), B (black)
        # Example: 4_H -> 4 of Heart, Z_R --> Red Joker
        self.code = code
        self.val = code.split("_")[0]
        self.suit = code.split("_")[1]

    def get_val(self):
        return self.val

    def get_val_rank(self):
        return VAL_MAP.get(self.val)

    def get_suit(self):
        return self.suit

    def get_val_suit(self):
        return self.val, self.suit

    def get_code(self):
        return self.code

    def get_nice_str(self):
        suit_mapping = {"S": "♠", "H": "♥", "C": "♣", "D": "♦"}
        if self.val == "Z":
            if self.suit == "R":
                return "Red Joker"
            else:
                return "Black Joker"
        else:
            return "{}{}".format(self.val, suit_mapping[self.suit])
    
    # Operator overload
    def __str__(self):
        #return self.get_code()
        return self.get_nice_str()

    def __eq__(self, c):
        return c.get_code() == self.get_code()

    def __repr__(self):
        return self.get_nice_str()

    def __hash__(self):
        return hash(self.code)

