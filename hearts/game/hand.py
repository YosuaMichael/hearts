import hearts.common.util as util


class Hand:
    def __init__(self):
        self.cards = []

    def init_hand(self, cards):
        self.cards.clear()
        self.cards += cards
        util.inplace_sort_cards(self.cards)

    def receive_card(self, card):
        self.cards.append(card)
        util.inplace_sort_cards(self.cards)

    def receive_cards(self, cards):
        self.cards.extend(cards)
        util.inplace_sort_cards(self.cards)

    def throw_card(self, card):
        self.cards.remove(card)
        return card

    def throw_cards(self, cards):
        for card in cards:
            self.cards.remove(card)
        return cards

    def get_hand(self):
        return self.cards

    # Overload operators
    def __str__(self):
        return str(util.sort_cards(self.cards))

    def __repr__(self):
        return str(util.sort_cards(self.cards))

    def __contains__(self, card):
        return card in self.cards

    def __iter__(self):
        self._iter_obj = iter(self.cards)
        return self

    def __next__(self):
        return next(self._iter_obj)

    def __len__(self):
        return len(self.cards)

    def __getitem__(self, index):
        return self.cards[index]


