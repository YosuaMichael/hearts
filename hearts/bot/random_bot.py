from hearts.bot.bot import Bot
import random


class RandomBot(Bot):
    """
    RandomBot will just chose a random card/s
    from the valid restriction choices
    """
    def choose_one_card(self, restriction, state_data):
        chosen_card = random.choice(restriction)
        return chosen_card

    def choose_multi_cards(self, restriction, n=3):
        chosen_cards = random.sample(restriction, n)
        return chosen_cards
