
class Bot:
    def __init__(self):
        pass

    def choose_one_card(self, restriction, state_data):
        """
        Function that implement logic on how to choose one card to play trick
        This function must return one card from restriction
        """
        raise NotImplementedError("A bot must implement choose_one_card function")

    def choose_multi_cards(self, restriction, n=3):
        """
        Function that implement logic on how to choose n cards to pass
        This function must return list of n cards from restriction
        """
        raise NotImplementedError("A bot must implement choose_multi_cards function")

    def ready_for_new_round(self, round_num):
        """
        Function that make sure the bot is ready for new round
        The bot can either clean memory, or ping the bot server if 
        it is a remote bot

        If not needed, no need to implement this function
        """
        pass

