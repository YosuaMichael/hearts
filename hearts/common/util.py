import logging
import hearts.common.config as config


def card_comparator(card):
    return (card.get_suit(), card.get_val_rank())


def sort_cards(cards):
    return sorted(cards, key=card_comparator)


def inplace_sort_cards(cards):
    cards.sort(key=card_comparator)
    return True


# Wrapping logging object so it can accept multiple input
class LoggerWrapper:
    LOG_HANDLER = logging.StreamHandler()
    LOG_HANDLER.setLevel(config.LOGGING_LEVEL)
    LOG_FORMAT = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    )
    LOG_HANDLER.setFormatter(LOG_FORMAT)
    def __init__(self, logger):
        self.logger = logger
        self.logger.setLevel(config.LOGGING_LEVEL)
        self.logger.addHandler(LoggerWrapper.LOG_HANDLER)

    def debug(self, *args):
        self.logger.debug(" ".join([str(x) for x in args]))

    def info(self, *args):
        self.logger.info(" ".join([str(x) for x in args]))

    def warn(self, *args):
        self.logger.warn(" ".join([str(x) for x in args]))

    def error(self, *args):
        self.logger.error(" ".join([str(x) for x in args]))

    def critical(self, *args):
        self.logger.critical(" ".join([str(x) for x in args]))


def create_logger(name):
    logger = logging.getLogger(name)
    return LoggerWrapper(logger)




