from hearts.game.game import HeartGame
from hearts.bot.random_bot import RandomBot
from hearts.game.player import Player

import hearts.game.input_handler as input_handler

if __name__ == "__main__":
    NUM_PLAYER = 4
    players = []

    # Players 0-3 will be played by RandomBot
    for i in range(0, NUM_PLAYER):
        rand_bot = RandomBot()
        ih = input_handler.BotInputHandler()
        ih.attach_bot(rand_bot)
        bot_player = Player(
                player_id="bot_{:2d}".format(i),
                name="Bot {}".format(i),
                input_handler=ih)
        players.append(bot_player)

    game = HeartGame(players, max_score=27)
    game.play()



