# Introduction
This repository is meant as a framework for building AI for card game called hearts.
We build the logic for the game in this repository as well as the interface for using the bot.
An example of randombot is provided in `hearts/bot/random_bot.py` and the code to use it can be found at `hearts/play_with_randombot.py`

# How to setup development env
First we should set the env variable using command:
```
source set_dev_env.sh
```

Then we can start developing using folder `hearts` as the main packages.
We can test whether the environment already set up properly by running
```
python hearts/play_with_randombot.py
```
